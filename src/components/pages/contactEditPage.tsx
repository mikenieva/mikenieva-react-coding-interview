import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { IPerson } from '@lib/models/person';
import { useContactEdit } from '@hooks/contacts/useContactEdit';

function ContactEditPage() {
  const { id } = useParams<{ id: string }>();
  console.log(id);
  const { contact: fetchedContact, isLoading, error, update } = useContactEdit();
  const [contact, setContact] = useState<IPerson | null>(null);

  useEffect(() => {
    setContact(fetchedContact);
  }, [fetchedContact]);

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    // Actualiza la información del contacto utilizando el hook useContactEdit
    if (contact) {
      update(contact);
    }
  };

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  if (!contact) {
    return <div>No contact found</div>;
  }

  return (
    <div>
      <h1>Edit Contact</h1>
      <form onSubmit={handleSubmit}>
        {/* Renderiza los campos del formulario para editar el contacto */}
        {/* Por ejemplo: */}
        <div>
          <label htmlFor="firstName">First Name:</label>
          <input
            id="firstName"
            type="text"
            value={contact.firstName}
            onChange={e => {
              const newFirstName = e.target.value;
              setContact(prevContact => ({ ...prevContact, firstName: newFirstName }));
            }}
          />
        </div>
        <div>
          <label htmlFor="lastName">Last Name:</label>
          <input
            id="lastName"
            type="text"
            value={contact.lastName}
            onChange={e => {
              const newLastName = e.target.value;
              setContact(prevContact => ({ ...prevContact, lastName: newLastName }));
            }}
          />
        </div>
        <div>
          <label htmlFor="email">Email:</label>
          <input
            id="email"
            type="email"
            value={contact.email}
            onChange={e => {
              const newEmail = e.target.value;
              setContact(prevContact => ({ ...prevContact, email: newEmail }));
            }}
          />
        </div>
        <button type="submit">Save Changes</button>
      </form>
    </div>
  );
}

export default ContactEditPage;

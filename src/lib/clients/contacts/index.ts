import { MockContactsClient } from './mockContactsClient';
import { ContactsClient } from './contactsClient';

export * from './contactTypes';

const contactsClient =
  process.env.NODE_ENV === 'development'
    ? new MockContactsClient(2)
    : new ContactsClient();

export { contactsClient };

import { faker } from '@faker-js/faker';

import { IPerson } from '@lib/models/person';
import { IContactsClient, IContactListArgs, IContactListResult } from './contactTypes';

function seedContacts(count: number) {
  const res: IPerson[] = [];

  for (let i = 0; i < count; i++) {
    res.push({
      id: faker.datatype.uuid(),
      email: faker.internet.email(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName()
    });
  }

  return res;
}

const CONTACTS_STORAGE_KEY = 'mockContactsStorage';

function loadContactsFromStorage(): IPerson[] | null {
  const storedContacts = localStorage.getItem(CONTACTS_STORAGE_KEY);
  if (storedContacts) {
    return JSON.parse(storedContacts);
  }
  return null;
}

function saveContactsToStorage(contacts: IPerson[]): void {
  localStorage.setItem(CONTACTS_STORAGE_KEY, JSON.stringify(contacts));
}

const delay = (ms: number): Promise<void> => new Promise(res => setTimeout(res, ms));

export class MockContactsClient implements IContactsClient {
  private readonly apiContacts: IPerson[];

  constructor(mockPersonsCount: number) {
    const storedContacts = loadContactsFromStorage();
    if (storedContacts) {
      this.apiContacts = storedContacts;
    } else {
      this.apiContacts = seedContacts(mockPersonsCount);
      saveContactsToStorage(this.apiContacts);
    }
  }

  async contactList(opts: IContactListArgs): Promise<IContactListResult> {
    const { pageNumber = 1, pageSize = 10 } = opts;

    await delay(1000);
    const skip = (pageNumber - 1) * pageSize;
    const take = pageSize;

    return {
      data: this.apiContacts.slice(skip, skip + take),
      totalCount: this.apiContacts.length
    };
  }

  async getContactById(id: string): Promise<IPerson> {
    const contact = this.apiContacts.find(contact => contact.id === id);
    if (!contact) {
      throw new Error(`Contact with id ${id} not found.`);
    }
    await delay(1000); // Simula la demora de una solicitud de red
    return contact;
  }

  async updateContact(id: string, update: IPerson): Promise<IPerson> {
    const contactIndex = this.apiContacts.findIndex(contact => contact.id === id);
    if (contactIndex === -1) {
      throw new Error(`Contact with id ${id} not found.`);
    }
    this.apiContacts[contactIndex] = update;
    await delay(1000); // Simula la demora de una solicitud de red
    return update;
  }
}

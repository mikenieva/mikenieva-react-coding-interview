import { IPerson } from '@lib/models/person';
import { IContactsClient, IContactListArgs, IContactListResult } from './contactTypes';

export class ContactsClient implements IContactsClient {
  contactList(_opts: IContactListArgs): Promise<IContactListResult> {
    throw new Error('Method not implemented.');
  }

  async getContactById(id: string): Promise<IPerson> {
    // Realiza una solicitud GET para obtener la información del contacto por ID
    // Suponiendo que tienes una API REST en "https://your-api.com/contacts/{id}"
    const response = await fetch(`https://your-api.com/contacts/${id}`);
    console.log('response', response);
    if (!response.ok) {
      throw new Error(`Error fetching contact with ID ${id}`);
    }
    const contact = await response.json();
    return contact;
  }

  async updateContact(id: string, update: IPerson): Promise<IPerson> {
    // Realiza una solicitud PUT para actualizar la información del contacto
    // Suponiendo que tienes una API REST en "https://your-api.com/contacts/{id}"
    const response = await fetch(`https://your-api.com/contacts/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(update)
    });
    if (!response.ok) {
      throw new Error(`Error updating contact with ID ${id}`);
    }
    const updatedContact = await response.json();
    return updatedContact;
  }
}

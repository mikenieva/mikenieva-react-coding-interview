import { useState, useEffect, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { IPerson } from '@lib/models/person';
// import { ContactsClient } from '@lib/clients/contacts/contactsClient';
import { contactsClient } from '@lib/clients/contacts';

export function useContactEdit() {
  const { id } = useParams<{ id: string }>();
  const [contact, setContact] = useState<IPerson | null>(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState<Error | null>(null);

  useEffect(() => {
    async function fetchContact() {
      try {
        setIsLoading(true);
        const fetchedContact = await contactsClient.getContactById(id);
        setContact(fetchedContact);
      } catch (err: any) {
        setError(err);
      } finally {
        setIsLoading(false);
      }
    }

    fetchContact();
  }, [id]);

  const handleUpdate = useCallback(async (updatedContact: IPerson) => {
    try {
      setIsLoading(true);
      await contactsClient.updateContact(updatedContact.id, updatedContact);
      setContact(updatedContact);
    } catch (err: any) {
      setError(err);
    } finally {
      setIsLoading(false);
    }
  }, []);

  return {
    contact,
    isLoading,
    error,
    update: handleUpdate // Asegúrate de retornar 'handleUpdate' con la propiedad 'update'
  };
}
